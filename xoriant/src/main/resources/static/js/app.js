var myApp = angular.module('hackerNews', ['ui.router','ui.bootstrap','ngStorage']);

myApp.config(function($stateProvider, $urlRouterProvider) {	

  $urlRouterProvider.otherwise('/home');
	  
  var helloState = {
    name: 'home',
    url: '/home',
    templateUrl: 'pages/home.html',
    controller: 'homeController'
  }
  
  $stateProvider.state(helloState);
});