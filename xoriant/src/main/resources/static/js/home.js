angular.module('hackerNews').controller('homeController', function($scope,$rootScope, $http,$localStorage) {

	$rootScope.news=[];
	$rootScope.searchText={};
	$rootScope.orderVariable={};

	$scope.totalItems = $scope.news.length;
	$scope.currentPage = 1;
	$scope.itemsPerPage = 5;

	$http.get('angular/file.json').then(function successCallback(response) {

		$rootScope.news=response.data;	
		$rootScope.news.splice(0, 1);
		$localStorage.news= $rootScope.news;
		$scope.setPagingData($scope.currentPage);
	}, function errorCallback(response) {

	});

	$scope.flipclass="flipchecked";
	$scope.flipCard=function(){
		if($scope.flipclass=="flipchecked")
			$scope.flipclass="flipnotchecked";
		else
			$scope.flipclass="flipchecked";
	}

	$scope.sort=function(){
		if($rootScope.orderVariable.data=="num_points")
			$rootScope.orderVariable.data="-num_points";
		else
			$rootScope.orderVariable.data="num_points";
	}

	$scope.nextPage=function(page){
			if(page<0)
				$scope.currentPage=1;
			else
				$scope.currentPage=page+1;
			
			$scope.setPagingData($scope.currentPage);
	}

	$scope.setPagingData= function (page) {
		var pagedData =  $localStorage.news.slice(
				(page - 1) * $scope.itemsPerPage,
				page * $scope.itemsPerPage
		);
		$rootScope.aCandidates = pagedData;
	}
});

